//2013KUCP1019 MANISHA GHUGHARWAL
package in.forsk.iiitk;

import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component

public class Manisha_2013KUCP1019_CoursesWrapper extends Activity {
	

	 private int sem;
	private String ccode = "";
	private String cname = "";

	public Manisha_2013KUCP1019_CoursesWrapper() {

	}

	public Manisha_2013KUCP1019_CoursesWrapper(JSONObject jObj) {
		try {
			System.out.println(jObj.toString());
			sem = jObj.getInt("Semester");
			ccode = jObj.getString("Code");
			cname = jObj.getString("Name");
		} catch (Exception e) {

		}

	}
	
	public int getsem() {
		return sem;

	}

	public void setsem(int sem) {
		this.sem = sem;
	}

	public String getccode() {
		return ccode;

	}

	public void setccode(String ccode) {
		this.ccode = ccode;
	}
	public String getcname() {
		return cname;

	}

	public void setcname(String cname) {
		this.cname = cname;
	}
	

}
