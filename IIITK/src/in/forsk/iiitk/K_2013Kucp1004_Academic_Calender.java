package in.forsk.iiitk;


import in.forsk.iiitk.adapter.K_2013kucp1004_AcademicListAdapter;
import in.forsk.iiitk.wrapper.K_2013kucp1004_AcademicWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

public class K_2013Kucp1004_Academic_Calender extends Activity {

	public final static String TAG = K_2013Kucp1004_Academic_Calender.class.getSimpleName();
	private Context context;

	private ActionBar mActionBar;
	ImageView mMenuIcon;

	ListView mAcademicList;
	K_2013kucp1004_AcademicListAdapter mAdapter;
	
	// Collection Framework
	ArrayList<K_2013kucp1004_AcademicWrapper> mAcademicDataList = new ArrayList<K_2013kucp1004_AcademicWrapper>();

	Button mBackBtn;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_k_2013kucp1004_academic__calender);
        
    	context = this;

		notify(TAG + " onCreate");
		
		mAcademicList= (ListView)findViewById(R.id.listView1);
		
		// Local File Parsing
//		try {
//			String json_string = getStringFromRaw(context, R.raw.academic_calender_code);
//
//			ArrayList<AcademicWrapper> mFacultyDataList = pasreLocalAcademicFile(json_string);
//
//			setAcademicListAdapter(mFacultyDataList);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		 parseRemoteAcademicFile("http://online.mnit.ac.in/iiitk/assets/academic_calendar.json");
    }

	public void printObject(K_2013kucp1004_AcademicWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "month : " + obj.getmonth());
		Log.d(TAG, "date : " + obj.getdate());
		Log.d(TAG, "title : " + obj.gettitle());
	}
	
		String getStringFromRaw(Context context, int resourceId) throws IOException {
			// Reading File from resource folder
			Resources r = context.getResources();
			InputStream is = r.openRawResource(resourceId);
			String statesText = convertStreamToString(is);
			is.close();

			Log.d(TAG, statesText);

			return statesText;
		}
    

    private String convertStreamToString(InputStream is)throws IOException {
			// TODO Auto-generated method stub
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
		}
    
    @Override
	protected void onStart() {
		super.onStart();

		// Toast.makeText(context, TAG + " onStart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onStart");
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		// Toast.makeText(context, TAG + " onRestart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onRestart");
	}

	// Called if the activity get visible again and the user starts interacting
	// with the activity again. Used to initialize fields, register listeners,
	// bind to services, etc.
	@Override
	protected void onResume() {
		super.onResume();

		// Toast.makeText(context, TAG + " onResume",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onResume");
	}

	// Called once another activity gets into the foreground. Always called
	// before the activity is not visible anymore. Used to release resources or
	// save application data. For example you unregister listeners, intent
	// receivers, unbind from services or remove system service listeners.
	@Override
	protected void onPause() {
		super.onPause();

		// Toast.makeText(context, TAG + " onPause",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onPause");
	}

	// Called once the activity is no longer visible. Time or CPU intensive
	// shut-down operations, such as writing information to a database should be
	// down in the onStop() method. This method is guaranteed to be called as of
	// API 11.
	@Override
	protected void onStop() {
		super.onStop();

		// Toast.makeText(context, TAG + " onStop",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onStop");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// Toast.makeText(context, TAG + " onDestroy",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onDestroy");
	}

	@SuppressLint("NewApi")
	private void notify(String methodName) {
		// String name = this.getClass().getName();
		// String[] strings = name.split("\\.");
		// Notification noti = new
		// Notification.Builder(this).setContentTitle(methodName + " " +
		// strings[strings.length -
		// 1]).setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher)
		// .setContentText(name).build();
		// NotificationManager notificationManager = (NotificationManager)
		// getSystemService(NOTIFICATION_SERVICE);
		// notificationManager.notify((int) System.currentTimeMillis(), noti);
	}

	private ArrayList<K_2013kucp1004_AcademicWrapper> pasreLocalAcademicFile(String json_string) {
		// TODO Auto-generated method stub
		ArrayList<K_2013kucp1004_AcademicWrapper> mFacultyDataList = new ArrayList<K_2013kucp1004_AcademicWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				K_2013kucp1004_AcademicWrapper facultyObject = new K_2013kucp1004_AcademicWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	


	private void setAcademicListAdapter(
			ArrayList<K_2013kucp1004_AcademicWrapper> mFacultyDataList) {
		// TODO Auto-generated method stub
		
		mAdapter = new K_2013kucp1004_AcademicListAdapter(context, mFacultyDataList);
		System.out.println(mFacultyDataList);
		mAcademicList.setAdapter(mAdapter);
		
	}
	

	private void parseRemoteAcademicFile(final String url) {
	
		// TODO Auto-generated method stub
		new AcademicParseAsyncTask().execute(url);
	}
	
	// Params, Progress, Result
	class AcademicParseAsyncTask extends AsyncTask<String, Integer, String> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(context);
			pd.setMessage("loading..");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String url = params[0];
			InputStream is = openHttpConnection(url);
			String response = "";
			try {
				response = convertStreamToString(is);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ArrayList<K_2013kucp1004_AcademicWrapper> mFacultyDataList = pasreLocalAcademicFile(result);
			setAcademicListAdapter(mFacultyDataList);

			if (pd != null)
				pd.dismiss();
		}
	}

	private InputStream openHttpConnection(String urlStr) {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}
	
	

    
}
