package in.forsk.iiitk.adapter;

import in.forsk.iiitk.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class CustomPagerAdapter extends PagerAdapter {

	Context mContext;
	LayoutInflater mLayoutInflater;
	TypedArray mResources;

	public CustomPagerAdapter(Context context, TypedArray resources) {
		mContext = context;
		mResources = resources;

		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mResources.length();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((LinearLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

		ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
		imageView.setImageResource(mResources.getResourceId(position, 0));

		container.addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}
}
