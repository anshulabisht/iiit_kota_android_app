package in.forsk.iiitk.adapter;

import java.util.ArrayList;

import com.androidquery.AQuery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import in.forsk.iiitk.R;
import in.forsk.iiitk.wrapper.k_2013kucp1009_VideoWrapper;

public class k_2013kucp1009_VideoListAdapter extends BaseAdapter {
	private final static String TAG = k_2013kucp1009_VideoListAdapter.class.getSimpleName();
	Context context;
	ArrayList<k_2013kucp1009_VideoWrapper> mFacultyDataList;
	LayoutInflater inflater;
	ViewHoder holder;

	AQuery aq;

	public k_2013kucp1009_VideoListAdapter(Context context, ArrayList<k_2013kucp1009_VideoWrapper> mFacultyDataList) {
		this.context = context;
		this.mFacultyDataList = mFacultyDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	@Override
	public int getCount() {
		return mFacultyDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mFacultyDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.k_2013kucp1009_video_adapter, null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say whem getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		k_2013kucp1009_VideoWrapper obj = mFacultyDataList.get(position);

		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(holder.profileIv).image(obj.getImage_url(), true, true, 200, 0);

		holder.TitleTv.setText(obj.getTitle());
		holder.videonoTv.setText(obj.getVideo_no());
		

		return convertView;
	}

	public static class ViewHoder {
		ImageView profileIv;
		TextView TitleTv, videonoTv;

		public ViewHoder(View view) {
			profileIv = (ImageView) view.findViewById(R.id.profileIv);

			TitleTv = (TextView) view.findViewById(R.id.titletv);
			videonoTv = (TextView) view.findViewById(R.id.videonotv);
			
		}
	}

}
