package in.forsk.iiitk.adapter;

import in.forsk.iiitk.R;
import in.forsk.iiitk.wrapper.K_2013kucp1004_AcademicWrapper;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class K_2013kucp1004_AcademicListAdapter extends BaseAdapter {

	private final static String TAG = K_2013kucp1004_AcademicListAdapter.class.getSimpleName();
	Context context;
	ArrayList<K_2013kucp1004_AcademicWrapper> mAcademicDataList;
	LayoutInflater inflater;
	ViewHoder holder;

	public K_2013kucp1004_AcademicListAdapter(Context context,
			ArrayList<K_2013kucp1004_AcademicWrapper> mAcademicDataList) {
		this.context = context;
		this.mAcademicDataList = mAcademicDataList;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mAcademicDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mAcademicDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// defining getView method

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_academic_calender_list,
					null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say whem getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		K_2013kucp1004_AcademicWrapper obj = mAcademicDataList.get(position);

		holder.month.setText(obj.getmonth());
		holder.date.setText(obj.getdate());
		holder.title.setText(obj.gettitle());

		return convertView;
	}

	// defining viewHolder class

	public static class ViewHoder {

		TextView month, date, title;

		public ViewHoder(View view) {

			month = (TextView) view.findViewById(R.id.month);
			date = (TextView) view.findViewById(R.id.date);
			title = (TextView) view.findViewById(R.id.title);
		}
	}
}
