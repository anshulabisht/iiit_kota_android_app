package in.forsk.iiitk.adapter;

import in.forsk.iiitk.R;
import in.forsk.iiitk.wrapper.k_2013kucp1009_VideoListWrapper;

import java.util.ArrayList;

import com.androidquery.AQuery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class k_2013kucp1009_SingleVideoAdapter extends BaseAdapter {
	private final static String TAG = k_2013kucp1009_SingleVideoAdapter.class.getSimpleName();
	Context context;
	ArrayList<k_2013kucp1009_VideoListWrapper> mVideoDataList;
	LayoutInflater inflater;
	ViewHoder holder;

	AQuery aq;

	public k_2013kucp1009_SingleVideoAdapter(Context context, ArrayList<k_2013kucp1009_VideoListWrapper> mVideoDataList) {
		this.context = context;
		this.mVideoDataList = mVideoDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	@Override
	public int getCount() {
		return mVideoDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mVideoDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_k_2013kucp1009_single_video_adapter, null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say when getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		System.out.println(mVideoDataList);
		k_2013kucp1009_VideoListWrapper obj = mVideoDataList.get(position);

		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(holder.thumbnail).image(obj.getthumbnail(), true, true, 200, 0);
       
		holder.TitleTv.setText(obj. getvideo_title());
		holder.descriptionTv.setText(obj.getdescription());
		holder.durationTv.setText(obj.getduration());

		return convertView;
	}

	public static class ViewHoder {
		ImageView thumbnail;
		TextView TitleTv, descriptionTv, durationTv;

		public ViewHoder(View view) {
			thumbnail = (ImageView) view.findViewById(R.id.thumbnail);

			TitleTv = (TextView) view.findViewById(R.id.videotitle);
			descriptionTv = (TextView) view.findViewById(R.id.description);
			durationTv = (TextView) view.findViewById(R.id.duration);
			
		}
	}

}
