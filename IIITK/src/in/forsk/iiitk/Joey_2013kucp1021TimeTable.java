package in.forsk.iiitk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import in.forsk.iiitk.adapter.Joey_2013kucp1021TimeTableListAdapter;
import in.forsk.iiitk.wrapper.Joey_2013kucp1021TimeTableWrapper;

public class Joey_2013kucp1021TimeTable extends Activity {

	ListView sem3;
	ListView sem5;
	private Context context;
	Joey_2013kucp1021TimeTableListAdapter mAdapter;
	ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableList = new ArrayList<Joey_2013kucp1021TimeTableWrapper>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joey_2013kucp1021_activity_time_table);
		context = this;
		sem3=(ListView) findViewById(R.id.sem_3);
		sem5=(ListView) findViewById(R.id.sem_5);
		 parseTimeTable(3,"http://192.168.229.1/iiitk/android/timetable.php?sem=3");
		 parseTimeTable(5,"http://192.168.229.1/iiitk/android/timetable.php?sem=5");
		
	}
	private InputStream openHttpConnection(String urlStr) {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}
	class TimeTableParseAsyncTask extends AsyncTask<String, Integer, String> {
       int semester;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(context);
			pd.setMessage("loading..");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String url = params[0];
			InputStream is = openHttpConnection(url);
			String response = "";
			try {
				response = convertStreamToString(is);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableDataList = pasreLocalFile(result);
			setTimeTableAdapter(semester,mTimeTableDataList);

			if (pd != null)
				pd.dismiss();
		}
	}
	public ArrayList<Joey_2013kucp1021TimeTableWrapper> pasreLocalFile(String json_string) {

		ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableDataList = new ArrayList<Joey_2013kucp1021TimeTableWrapper>();
		try {
			System.out.println(json_string);
			// Converting multipal json data (String) into Json array
			JSONArray timetableArray = new JSONArray(json_string);
			Log.d("joey", timetableArray.toString());
			// Iterating json array into json objects
			for (int i = 0; timetableArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject timetableJsonObject = timetableArray.getJSONObject(i);

				// Design patterns
				Joey_2013kucp1021TimeTableWrapper timetableObject = new Joey_2013kucp1021TimeTableWrapper(timetableJsonObject);

				

				mTimeTableDataList.add(timetableObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mTimeTableDataList;
	}
	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}
	private void parseTimeTable(final int sem,final String url) {
		// TODO Auto-generated method stub
		TimeTableParseAsyncTask tt=new TimeTableParseAsyncTask();
		tt.semester=sem;
		tt.execute(url);
		
		
	}

	public void setTimeTableAdapter(int semester,ArrayList<Joey_2013kucp1021TimeTableWrapper> mTimeTableDataList) {
		
		mAdapter = new Joey_2013kucp1021TimeTableListAdapter(context, mTimeTableDataList);
		if(semester==3)  sem3.setAdapter(mAdapter);
		if(semester==5)  sem5.setAdapter(mAdapter);
	}
	

}
