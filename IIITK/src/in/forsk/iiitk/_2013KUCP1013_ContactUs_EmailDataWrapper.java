package in.forsk.iiitk;

/**
 * Created by mnit on 29/10/15.
 */
public class _2013KUCP1013_ContactUs_EmailDataWrapper {

    private String to;
    private String subject;
    private String body;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


}
