package in.forsk.iiitk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class k_2013kucp1009_Video extends Activity {

	
	                         /*this class for click on video */
	
	MediaPlayer mediaPlayer;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean pausing = false;;
   private ProgressDialog progressDialog;
    
    private MediaController mediaController;
    WebView displayVideo;
    String str;
	
	
	private final static String TAG = k_2013kucp1009_VideoGallery.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k_2013kucp1009_video);
		
	
		
		TextView tv1,tv2,tv3,tv4;
		
	    

		
		
		displayVideo = (WebView) findViewById(R.id.webView);
		
		tv1 = (TextView) findViewById(R.id.toolbarHeader);
		tv2 = (TextView) findViewById(R.id.videotitle);
		tv3 = (TextView) findViewById(R.id.duration);
		tv4 = (TextView) findViewById(R.id.description);
		
		Bundle bundle = getIntent().getExtras();
		 str = bundle.getString("video_url");
		String str1 = bundle.getString("video_title");
		String str2 = bundle.getString("duration");
		String str3 = bundle.getString("description");
		
		tv1.setText(str1);
		tv2.setText(str1);
		tv3.setText(str2);
		tv4.setText(str3);
		
	
	    
	    
	    
	    
	    
ImageView bk=(ImageView)findViewById(R.id.back);
		
		bk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext() , k_2013kucp1009_VideoList.class);
				startActivity(intent);
				 finish();
			}
		});
	    
	    
	    
		
		
		
		
		
		
		  String frameVideo = "<html><body>Youtube video .. <br> <iframe width=\"320\" height=\"250\" src=\""+str+"\"frameborder=\"0\" allowfullscreen></iframe></body></html>";

	         displayVideo = (WebView)findViewById(R.id.webView);
	        displayVideo.setWebViewClient(new WebViewClient(){
	            @Override
	            public boolean shouldOverrideUrlLoading(WebView view, String url) {
	                return false;
	            }
	        });
	        WebSettings webSettings = displayVideo.getSettings();
	        webSettings.setJavaScriptEnabled(true);
	        displayVideo.loadData(frameVideo, "text/html", "utf-8");
		
		
			
	}
	       
	
	
	
}
