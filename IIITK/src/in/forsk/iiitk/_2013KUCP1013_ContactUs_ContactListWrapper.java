package in.forsk.iiitk;

/**
 * Created by student on 28-Oct-15.
 */
public class _2013KUCP1013_ContactUs_ContactListWrapper {

    private String facultyName;
    private String facultyImage;
    private String facultyContact;

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }



    public String getFacultyImage() {
        return facultyImage;
    }

    public void setFacultyImage(String facultyImage) {
        this.facultyImage = facultyImage;
    }



    public String getFacultyContact() {
        return facultyContact;
    }

    public void setFacultyContact(String facultyContact) {
        this.facultyContact = facultyContact;
    }


}
