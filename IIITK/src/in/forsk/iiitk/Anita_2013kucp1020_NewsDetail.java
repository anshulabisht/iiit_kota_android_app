//package in.forsk.iiitk;
//
//import in.forsk.iiitk.wrapper.Anita_2013kucpanita_1020NewsDetailWrapper;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import com.androidquery.AQuery;
//
//import android.annotation.SuppressLint;
//import android.app.ActionBar;
//import android.app.Activity;
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.res.Resources;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//public class Anita_2013kucpanita_1020_NewsDetail extends Activity {
//	static String Title, Subtitle, Image, Detail, Id;
//	LayoutInflater inflater;
//	AQuery aq;
//	public final static String TAG = FacultyProfile.class.getSimpleName();
//	private Context context;
//
//	// Collection Framwork
//	ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper> mNewsDetailDataList = new ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper>();
//
//	Button mBackBtn;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_news_detail);
//
//		context = this;
//
//		Toast.makeText(context, TAG + " onCreate", Toast.LENGTH_SHORT).show();
//		notify(TAG + " onCreate");
//
//		System.out.println("http://192.168.229.1/iiitk/android/newsdetail.php?id=" + Id);
//		parseNewsDetail("http://192.168.229.1/iiitk/android/newsdetail.php?id=" + Id);
//
//	}
//
//	private String getStringFromRaw(Context context, int resourceId) throws IOException {
//		// Reading File from resource folder
//		Resources r = context.getResources();
//		InputStream is = r.openRawResource(resourceId);
//		String statesText = convertStreamToString(is);
//		is.close();
//		return statesText;
//	}
//
//	private String convertStreamToString(InputStream is) throws IOException {
//		// Converting input stream into string
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		int i = is.read();
//		while (i != -1) {
//			baos.write(i);
//			i = is.read();
//		}
//		return baos.toString();
//	}
//
//	@SuppressLint("NewApi")
//	private void notify(String methodName) {
//		String name = this.getClass().getName();
//		String[] strings = name.split("\\.");
//		Notification noti = new Notification.Builder(this)
//				.setContentTitle(methodName + " " + strings[strings.length - 1]).setAutoCancel(true)
//				.setSmallIcon(R.drawable.ic_launcher).setContentText(name).build();
//		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//		notificationManager.notify((int) System.currentTimeMillis(), noti);
//	}
//
//	public ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper> pasreLocalFacultyFile(String json_string) {
//
//		ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper> mNewsDetailDataList = new ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper>();
//		try {
//			// Converting multipal json data (String) into Json array
//			JSONArray facultyArray = new JSONArray(json_string);
//			Log.d(TAG, facultyArray.toString());
//			// Iterating json array into json objects
//			for (int i = 0; facultyArray.length() > i; i++) {
//
//				// Extracting json object from particular index of array
//				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);
//
//				// Design patterns
//				Anita_2013kucpanita_1020NewsDetailWrapper facultyObject = new Anita_2013kucpanita_1020NewsDetailWrapper(
//						facultyJsonObject);
//
//				mNewsDetailDataList.add(facultyObject);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return mNewsDetailDataList;
//	}
//
//	public void parseNewsDetail(final String url) {
//		new NewsDetailParseAsyncTask().execute(url);
//	}
//
//	// Params, Progress, Result
//	class NewsDetailParseAsyncTask extends AsyncTask<String, Integer, String> {
//
//		ProgressDialog pd;
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//
//			pd = new ProgressDialog(context);
//			pd.setMessage("loading..");
//			pd.setCancelable(false);
//			pd.show();
//		}
//
//		@Override
//		protected String doInBackground(String... params) {
//			String url = params[0];
//			InputStream is = openHttpConnection(url);
//			String response = "";
//			try {
//				response = convertStreamToString(is);
//
//				onProgressUpdate(0);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			return response;
//		}
//
//		@Override
//		protected void onProgressUpdate(Integer... values) {
//			// TODO Auto-generated method stub
//			super.onProgressUpdate(values);
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			super.onPostExecute(result);
//			ArrayList<Anita_2013kucpanita_1020NewsDetailWrapper> mNewsDetailDataList = pasreLocalFacultyFile(result);
//
//			Subtitle = mNewsDetailDataList.get(0).getSubtitle();
//			System.out.println("reached" + Subtitle);
//			Title = mNewsDetailDataList.get(0).getTitle();
//			Detail = mNewsDetailDataList.get(0).getDetail();
//			Image = mNewsDetailDataList.get(0).getImage();
//			aq = new AQuery(context);
//			TextView title = (TextView) findViewById(R.id.NewsTitle);
//			title.setText(Title);
//			TextView subtitle = (TextView) findViewById(R.id.NewsSubtitle);
//			subtitle.setText(Subtitle);
//			TextView detail = (TextView) findViewById(R.Anita_2013kucpanita_1020_NewsDetail.NewsDetail);
//			detail.setText(Detail);
//			ImageView image = (ImageView) findViewById(R.id.NewsImage);
//
//			aq.id(image).image("http://192.168.229.1/iiitk/assets/images/news/" + Image, true, true, 200, 0);
//
//			if (pd != null)
//				pd.dismiss();
//		}
//	}
//
//	private InputStream openHttpConnection(String urlStr) {
//		InputStream in = null;
//		int resCode = -1;
//
//		try {
//			URL url = new URL(urlStr);
//			URLConnection urlConn = url.openConnection();
//
//			if (!(urlConn instanceof HttpURLConnection)) {
//				throw new IOException("URL is not an Http URL");
//			}
//
//			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
//			httpConn.setAllowUserInteraction(false);
//			httpConn.setInstanceFollowRedirects(true);
//			httpConn.setRequestMethod("GET");
//			httpConn.connect();
//
//			resCode = httpConn.getResponseCode();
//			if (resCode == HttpURLConnection.HTTP_OK) {
//				in = httpConn.getInputStream();
//			}
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return in;
//	}
//
//}>>>>>>>remotes/pintojoey/iiit_kota_android_app/master
