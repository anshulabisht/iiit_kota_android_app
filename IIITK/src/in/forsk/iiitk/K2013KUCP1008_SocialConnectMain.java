package in.forsk.iiitk;

import in.forsk.iiitk.wrapper.K2013KUCP1008_SocialConnectWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class K2013KUCP1008_SocialConnectMain extends Activity {

	String social_connect_json_url="http://online.mnit.ac.in/iiitk/assets/social_connect_code.txt";
    String connectUrl="";
    String openInApp="";
    Context context;
    ArrayList<K2013KUCP1008_SocialConnectWrapper> mSocialConnectUrlList;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_k2013_kucp1008__social_connect_main);
        context=this;
        new CustomAsyncTask().execute();
        
    }


   

    private String convertStreamToString(InputStream is)throws IOException {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		int i=is.read();
		while(i!=-1)
		{
			baos.write(i);
			i=is.read();
		}
		return baos.toString();
	}
	
	
	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}
    
    public void onClickfb(View v){
		getUrl("fb");
		if(openInApp.equals("no")){
			//Open the page in browser
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
        startActivity(intent);
		}
		else{
			//Open in page in app
			Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
			Log.e("same app","");
			i.putExtra("url",connectUrl); //send url to next activity
			startActivity(i);
		}
	}
	
	public void onClickinsta(View v){
		getUrl("insta");
		if(openInApp.equals("no")){
			//Open the page in browser
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
            startActivity(intent);
			}
			else{
				//Open the page in app
				Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
				i.putExtra("url",connectUrl);
				startActivity(i);
			}
		}
	public void onClickgoogle(View v){
		getUrl("google");
		if(openInApp.equals("no")){
			//Open the page in browser
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
            startActivity(intent);
			}
			else{
				//Open the page in app
				Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
				i.putExtra("url",connectUrl);
				startActivity(i);	
			}
	}
	
	public void onClickyou(View v){
		getUrl("you");
		if(openInApp.equals("no")){
			//Open the page in browser
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
            startActivity(intent);
			}
			else{
				//Open the page in app
				Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
				i.putExtra("url",connectUrl);
				startActivity(i);
			}
	}
	public void onClicktwi(View v){
		getUrl("twi");
		if(openInApp.equals("no")){
			//Open the page in browser
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
            startActivity(intent);
			}
			else{
				//Open the page in app
				Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
				i.putExtra("url",connectUrl);
				startActivity(i);
			}
	}
	
	public void onClicklinked(View v){
		getUrl("linkedin");
		if(openInApp.equals("no")){
			//Open the page in browser
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectUrl));
            startActivity(intent);
			}
			else{
				//Open the page in app
				Intent i=new Intent(this,K2013kucp1008_SocialConnectPage.class);
				i.putExtra("url",connectUrl);
				startActivity(i);
			}	
	}
	
	public void getUrl(String connectId){
		for (int i = 0; i < mSocialConnectUrlList.size(); i++) {
			if((mSocialConnectUrlList.get(i).getConnectId()).equals(connectId))
				{
				connectUrl=mSocialConnectUrlList.get(i).getConnectUrl();
				 openInApp=mSocialConnectUrlList.get(i).getOpenInApp();
				}
		}
	}
	
	
	class CustomAsyncTask extends AsyncTask<Void, Void, String> {

		String url;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			url =social_connect_json_url;
			Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String response = "";
			try {
				response = openHttpConnection(url);
				
				try {
					Thread.sleep(5000);
				    } 
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				                                }

			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

				mSocialConnectUrlList = new ArrayList<K2013KUCP1008_SocialConnectWrapper>();
				JSONArray socialUrlArray;
				try {
					socialUrlArray = new JSONArray(result);
					// Iterating json array into json objects
					for (int i = 0; socialUrlArray.length() > i; i++) {
						// Extracting json object from particular index of array
						JSONObject urlJsonObject = socialUrlArray.getJSONObject(i);
						K2013KUCP1008_SocialConnectWrapper urlObject = new K2013KUCP1008_SocialConnectWrapper(urlJsonObject);
						mSocialConnectUrlList.add(urlObject);
						}
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
			Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();
	}
}
}
